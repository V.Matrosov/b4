#!/usr/bin/env python3

import cgi
import html
import sys
import codecs
import datetime
import psycopg2
import re
from psycopg2 import sql

sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())

form = cgi.FieldStorage()
d = {'cookie': form}
text1 = form.getvalue("name", "")
text1 = html.escape(text1)
d['name'] = text1
text2 = form.getvalue("email")
if not text2 is None:
    d['email'] = text2
else:
    d['email'] = ''
    text2 = ''
data = datetime.datetime.strptime(form.getvalue("date"), "%Y-%m-%d")
d['data'] = data.strftime("%Y-%m-%d")
pol = form.getvalue("pol")
d['pol'] = pol
status = int(form.getvalue("status"))
d['status'] = status
forma = form.getvalue("forma")
d['forma'] = forma
if form.getvalue('text'):
    bio = form.getvalue('text')
    d['bio'] = bio
else:
    bio = ""
    d['bio'] = ''

if form.getvalue("box"):
    box = True
    d['check'] = 'on'
else:
    box = False
    d['check'] = 'off'
f = False

print('Set-cookie: text1=%s', d['name'])
print('Set-cookie: text2=%s', d['email'])
print('Set-cookie: data=%s', d['data'])
print('Set-cookie: pil=%s', d['pol'])
print('Set-cookie: status=%s', d['status'])
print('Set-cookie: forma=%s', d['forma'])
print('Set-cookie: bio=%s', d['bio'])
print('Set-cookie: box=%s', d['check'])

print("Content-type: text/html")
print()
print('''<DOCTYPE html>
         <html lang="ru">
         <head>
         <title>backend-4</title>
         <meta charset="UTF-8">
         <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
         </head>
         <body>
         <form action="/cgi-bin/form1.py" method="post">
         ''')

if text1 == "":
    print('''
    <div class="bg-danger">
    <label>Имя:</label>''')
    print('<input type="text" name="name" value="' + d['name'] + '"></label>')
    print('''<br>
       </div>''')
    f = True
else:

    print('''
        <label>Имя:</label>''')
    print('<input type="text" name="name" value="' + d['name'] + '"><br>')

if text2 == "":
    print('''
        <div class="bg-danger">
        <label>Email:</label>''')
    print('<input type="email" name="email" value="' + text2 + '"></label>')
    print('''<br>
    </div>''')
    f = True
else:
    print('''
            <label>Email:</label>''')
    print('<input type="email" name="email" value="' + text2 + '"><br>')

print('<label> Дата рождения: </label>')
print('<input type="date" id="date" name="date" value="' + d['data'] + '"><br>')
print('<label>Пол:</label>')
if pol == "Мужской":
    print(''' <label>   <input type="radio" name="pol" value="Мужской" checked="checked"> М </label>
              <label>   <input type="radio" name="pol" value="Женский"> Ж </label>
              <br>
              <label>Количесво конечностей:</label>
    ''')
else:
    print('''

       <label>   <input type="radio" name="pol" value="Мужской" > М </label>
       <label>   <input type="radio" name="pol" value="Женский" checked="checked"> Ж </label>
       <br>
       <label>Количесво конечностей:</label>
    ''')

for i in range(2, 11, 4):
    if status == i:
        print(
            '<label> <input type="radio" name="status" value="' + str(i) + '" checked="checked">' + str(i) + '</label>')
    else:
        print('<label> <input type="radio" name="status" value="' + str(i) + '">' + str(i) + '</label>')

raz = ' '.join(forma)
print('''<br>
       <label>Сверхспособности:</label>
       <select multiple name="forma">''')
if re.search("бессмертие", raz):
    print('<option value="бессмертие" selected="selected">бессмертие</option>')
else:
    print('<option value="бессмертие">бессмертие</option>')
if re.search("прохождение сквозь стены", raz):
    print('<option value="прохождение сквозь стены" selected="selected">прохождение сквозь стены</option>')
else:
    print('<option value="прохождение сквозь стены">прохождение сквозь стены</option>')
if re.search("левитация", raz):
    print('<option value="левитация" selected="selected">левитация </option>')
else:
    print('<option value="левитация">левитация</option>')

print('''</select>
       <br>
''')
if bio == "":
    print('''
        <div class="bg-danger">
         <label>Биография:</label>''')
    print('<textarea name="text" value="' + d['bio'] + '">' + d['bio'] + '</textarea>')
    print(''''<br>
        </div>''')
    f = True
else:
    print('<label>Биография:</label>')
    print('<textarea name="text" value="' + d['bio'] + '">' + d['bio'] + '</textarea><br>')
if box:
    print('''
                    <label>
             <input type="checkbox" name="box" checked="checked">
             с контрактом ознакомлен
             </label>
             <br>
         <button>
        Отправить
         </button>
   </form>
    ''')
else:
    print('''      <label class="bg-danger">
                 <input type="checkbox" name="box">
                 с контрактом ознакомлен
                 </label>
                 <br>
             <button>
            Отправить
             </button>
       </form>
        ''')
    f = True
if f:
    print('''<p>Пожалуйста, заполните форму заново</p>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</body>
</html>''')
    exit()
else:
    print('''<p>Форма успешно отправлена</p>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    </body>
    </html>''')

conn = psycopg2.connect(
    dbname='postgres',
    user='myuser',
    password='mypassword',
    host='localhost'
)
with conn.cursor() as cursor:
    conn.autocommit = True
    values = [
        (0, text1, text2, data, pol, status, forma, bio, box),
    ]
    insert = sql.SQL(
        'INSERT INTO form (id_person, first_name, email, nowdate, sex, kolvo, abilities, bio, checkbox) VALUES {}').format(
        sql.SQL(',').join(map(sql.Literal, values))
    )
    cursor.execute(insert)
conn.close()